## Register a Git-Lab Runner
```
sudo gitlab-runner register
```
## Create a maven project
```
mvn archetype:generate
```
Note: 1637 was the mavin quickstart archetype

Else you can use the following command to create an archetype

```
 mvn archetype:generate -DgroupId=com.gaurav.service -DartifactId=testapp -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```